package { 'nginx':
  ensure => installed,
}

# Define NGINX service
service { 'nginx':
  ensure => running,
  enable => true,
}

# Define NGINX configuration file
file { '/etc/nginx/nginx.conf':
  ensure  => file,
  owner   => 'root',
  group   => 'root',
  mode    => '0644',
  notify  => Service['nginx'],
}

# Define default index.html file
file { '/usr/share/nginx/html/index.html':
  ensure  => file,
  owner   => 'nginx',
  group   => 'nginx',
  mode    => '0644',
  content => "<html><body><h1>Welcome to NGINX!</h1></body></html>",
  require => Package['nginx'],
}
