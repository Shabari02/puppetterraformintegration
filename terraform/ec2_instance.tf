
provider "aws" {
  region = "us-east-1"
}


# Create a security group
resource "aws_security_group" "sg_ec2_2" {
  name        = "sg_ec2_2"
  description = "Security group for EC2"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "my_ec2" {
  ami                    = "ami-0440d3b780d96b29d"
  instance_type          = "t2.micro"
  key_name               = "MYEC2SSH"
  vpc_security_group_ids = [aws_security_group.sg_ec2_2.id]

  tags = {
    Name = "my_ec2"
  }

  root_block_device {
    volume_size = 30
    volume_type = "gp2"
  }



  provisioner "remote-exec" {
    inline = [
      "sudo yum update",
      "sudo yum install -y http://yum.puppetlabs.com/puppet8-release-amazon-2023.noarch.rpm",
      "sudo yum install -y puppet-agent",
      "sudo systemctl start puppet",
      "sudo systemctl enable puppet",
      "sudo /opt/puppetlabs/bin/puppet module install puppet-nginx --version 5.0.0"
    ]

    connection {
      type        = "ssh"
      user        = "ec2-user"                                    
      private_key = file("C:/Users/lksha/Downloads/MYEC2SSH.pem") 
      host        = aws_instance.my_ec2.public_ip               
    }
  }

}

resource "null_resource" "send_file" {
  depends_on = [aws_instance.my_ec2]
  provisioner "file" {
    source      = "E:/Worldline/proj/worldline_test3/puppetterraformintegration/puppet/nginx.pp"
    destination = "/home/ec2-user/etc/puppetlabs/code/environments/production/manifests/"
    connection {
      type        = "ssh"
      user        = "ec2-user"
      private_key = file("C:/Users/lksha/Downloads/MYEC2SSH.pem")
      host        = aws_instance.my_ec2.public_ip
    }
  }
}

resource "null_resource" "trigger_puppet" {
  depends_on = [aws_instance.my_ec2]
  provisioner "remote-exec" {
    inline = [
      "puppet --version",
      "cd /etc/puppetlabs/code/environments/production/manifests",
      "sudo /opt/puppetlabs/bin/puppet apply nginx.pp",
      "sudo systemctl status nginx",
      "sudo cat /etc/nginx/nginx.conf"
    ]
  }
  connection {
    type        = "ssh"
    user        = "ec2-user"
    private_key = file("C:/Users/lksha/Downloads/MYEC2SSH.pem") # Path to SSH private key
    host        = aws_instance.my_ec2.public_ip
  }
}



